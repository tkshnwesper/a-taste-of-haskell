export { default as theme } from './theme'

import start from './deck.mdx'
import intro from './slides/1_intro/index.mdx'
import features from './slides/2_features/index.mdx'
import functions from './slides/3_functions/index.mdx'
import lists from './slides/4_lists/index.mdx'
import types from './slides/5_types/index.mdx'

export default [
  ...start,
  ...intro,
  ...features,
  ...functions,
  ...lists,
  ...types,
]
