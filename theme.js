import { dark as theme } from 'mdx-deck/themes'
import okaidia from 'react-syntax-highlighter/styles/prism/okaidia'
import { haskell } from 'react-syntax-highlighter/languages/prism'


export default {
  ...theme,

  prism: {
    style: okaidia,
    languages: {
      haskell
    }
  },

  colors: {
    ...theme.colors,
  },

  ul: {
    lineHeight: '1.7em'
  },
  p: {
    lineHeight: '1.7em'
  }
}
